package socket;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.InetAddress;

import datos.*;
import romano.ConversorRomano;
import utils.LogUtil;

public class ConversorRomanoThread extends Thread {
    
	//Referencia al cliente que sera atendido
    private Socket cliente;
    
    public ConversorRomanoThread(Socket cliente) {
        this.cliente = cliente;
    }
    
    @Override
    public void run() {
    	LogUtil.INFO("Nueva peticion recibida...");
        //TODO: Pendiente de implementacion de logica que parsea peticion, procesa request y almacena resultado en la bd
    	
    	//LogUtil.INFO("Finalizando atencion de la peticion recibida...");
    
    	try {
                        
            //cuando llegue a esta linea, es porque la anterior se ejecuto y avanzo
            //lo cual quiere decir que algun cliente se conecto a este servidor
            //obtenemos el OutputStream del cliente, para escribirle algo
            OutputStream clientOut = cliente.getOutputStream();
            PrintWriter pw = new PrintWriter(clientOut, true);
            
            //obtenemos el InputStream del cliente, para leer lo que nos dice
            InputStream clientIn = cliente.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(clientIn));
            
            //Leemos la primera linea del mensaje recibido
            String mensajeRecibido = br.readLine();
         // Variables para cargar la tabla
         			String nombre;
         			String ipCli;
         			int numero;
         			String responseText;
         			LogConversiones bean;
         			InetAddress address;

            
           //verificamos que viene en el mensaje
            if(mensajeRecibido != null){
            	if (mensajeRecibido.trim().equalsIgnoreCase("fin")){
            		//Nos enviaron la palabra fin, debemos pasar el servidor rompiendo el while
            		pw.println("Fin del proceso!.");
					System.exit(0);
            	//break;	
            	
            	} else if (mensajeRecibido.trim().startsWith("conv")) {
					// Nos enviaron la palabra conv debemos sacar el nro.
					String strnum = mensajeRecibido.substring(4);
					numero = Integer.parseInt(strnum.trim());
					try {
						responseText = "El valor convertido es: " + ConversorRomano.decimal_a_romano(numero);
						pw.println(responseText);
					} catch (Exception e) {
						responseText = "Hubo un error: " + e.getMessage();
					}
										
					//ipCli = cliente.getRemoteSocketAddress().toString();
					nombre = this.nombreThread();
					//Cargamos el bean
					//bean = new LogConversiones(nombre, ipCli, numero, responseText);
					LogConversiones lc = new LogConversiones(this.getName(),cliente.getInetAddress().getHostAddress(),numero, responseText);
					//Se guarda el Log de Conversiones
					LogConversionesManager lcm = new LogConversionesManager();
					lcm.insertarNuevoRegistro(lc);
					pw.println("Procesado");
				}
				//pw.println("Mensaje recibido no valido, reintente por favor!");
			}
            	
            
         
            
         // cerramos conexion con el cliente luego de temrinar el trabajo
         			clientIn.close();
         			clientOut.close();
         			cliente.close();
         		} catch (IOException ie) {
         			System.out.println("Error al procesar Thread. ");
                 }
             	LogUtil.INFO("Finalizando atencion de la peticion recibida...");
             }
             
             public String nombreThread(){
             	LogConversionesManager contador = new LogConversionesManager();
             	return "Converción " + contador.getCantidadRegistros();
             }
             
         }
