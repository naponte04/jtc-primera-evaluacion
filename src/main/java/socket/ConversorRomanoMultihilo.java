package socket;

import java.net.*;
import java.io.*;
import utils.LogUtil;

/**
 * Clase ConversorRomanoServer Multihilo
 * Curso de Programacion Java
 * @author Derlis Zarate
 */
public class ConversorRomanoMultihilo {

    public static void main(String[] args) {
        ServerSocket servidor = null;
        Socket cliente;
        ConversorRomanoThread hilo;
        
        try {
            //Abre un servidor socket local en el puerto 6400
        	LogUtil.INFO("Iniciando socket server en puerto 6400");
            servidor = new ServerSocket(6400);
        } catch (IOException ie) {
            System.err.println("Error al abrir socket. " + ie.getMessage());
            System.exit(1);
        }
        
        LogUtil.INFO("Socket iniciado, se atienden peticiones..");
        // Pendiente de implementacion del procesador de peticiones
        while (true){
        	//el servidor socket queda esperando indefinidamente conexiones
            //cuando algun cliente se conecte, en el objeto "cliente" tendremos
            //una referencia al cliente conectado
            try {
				cliente = servidor.accept();
				
				hilo = new ConversorRomanoThread(cliente);
				hilo.start();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
    }
    
    

}