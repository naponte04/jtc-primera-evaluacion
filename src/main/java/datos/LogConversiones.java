package datos;

import java.util.Date;



/**
 * Clase LogConversiones. Representa a 1 registro de la tabla LOG_CONVERSIONES
 * Curso de Programacion Java
 * @author Derlis Zarate
 */
public class LogConversiones {
    
    // Crear estructura de la clase, constructor, getters y setters
	  private Integer idMensaje;
	  private String nombreThread;
	  private String ipCliente;
	  private Date fechaHora;
	  private Integer msgRequest;
	  private String msgResponse;
	  
 public LogConversiones(String nombreThread, String ipCliente, int msgRequest, String msgResponse){
	 
	 this.setNombreThread(nombreThread);
	 this.setIpCliente(ipCliente);
	 this.fechaHora = new Date();
	 this.setMsgRequest(msgRequest);
	 this.setMsgResponse(msgResponse);
	 
 }
	  
	public Integer getIdMensaje() {
		return idMensaje;
	}
	public void setIdMensaje(Integer idMensaje) {
		this.idMensaje = idMensaje;
	}
	public String getNombreThread() {
		return nombreThread;
	}
	public void setNombreThread(String nombreThread) {
		this.nombreThread = nombreThread;
	}
	public String getIpCliente() {
		return ipCliente;
	}
	public void setIpCliente(String idCliente) {
		this.ipCliente = idCliente;
	}
	public Date getFechaHora() {
		return fechaHora;
	}
	public void setFechaHora(Date fechaHora) {
		this.fechaHora = fechaHora;
	}
	public Integer getMsgRequest() {
		return msgRequest;
	}
	public void setMsgRequest(Integer msgRequest) {
		this.msgRequest = msgRequest;
	}
	public String getMsgResponse() {
		return msgResponse;
	}
	public void setMsgResponse(String msgResponse) {
		this.msgResponse = msgResponse;
	}
     
	  
} //Fin de clase

